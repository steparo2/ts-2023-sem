package models;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage {
    private WebDriver driver;


    @FindBy(xpath="//*[@id=\"cart_item_list_form_submit\"]")
    private WebElement goToPaymentPageButton;

    @FindBy(css = "body > div.web__in.web__in--order-process > div:nth-child(2) > div > div > div > form > table > tfoot > tr > td.table-cart__cell.table-cart__cell--total-price.table-cart__cell--final-price.js-cart-total-price > span")
    private WebElement totalPrice;



    public CartPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PaymentPage goToPaymentPage(){
        waitForElementVisibility(goToPaymentPageButton);
        goToPaymentPageButton.click();
        return new PaymentPage(driver);
    }

    public void waitForElementVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public WebElement getTotalPrice() {
        return totalPrice;
    }

}
