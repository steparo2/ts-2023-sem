package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AllBooksPage {
    private WebDriver driver;
    @FindBy(css = "#js-fixed-header > div.web__header > div > header > div.header__logo > a")
    private WebElement mainPageElement;

    @FindBy(css = "#js-window > div.js-window-content.window-popup__in > button")
    private WebElement continueShopButton;

    @FindBy(css = "#js-window > div.js-window-content.window-popup__in > a")
    private WebElement goToCartButton;

    @FindBy(css = "#js-cart-box > div.cart > a")
    private WebElement cartNav;

    @FindBy(xpath = "//*[@id=\"frm-filter\"]/div[2]/div[1]")
    private WebElement priceButton;

    @FindBy(css = "#product_filter_form_minimalPrice")
    private WebElement minimalPriceInput;

    @FindBy(css = "#product_filter_form_maximalPrice")
    private WebElement maximalPriceInput;

    @FindBy(css = "body > div.web__in > div:nth-child(2) > div > div > div > div.box-list > div.js-product-list-with-filter > div > div.box-filter-values.hidden-print.js-filter-values > span.box-filter-values__item.box-filter-values__item--value.box-filter-values__item--clearable.js-product-filter-reset-parameter-value")
    private WebElement priceRangeString;

    @FindBy(xpath = "//*[@id=\"frm-filter\"]/div[3]/div[1]")
    private WebElement availabilityOfBooksButton;

    @FindBy(css = "input#product_filter_form_immediateToSend")
    private WebElement availabilityCheckBox;

    @FindBy(css = "button.box-filter__item__submit")
    private WebElement enterAvailabilityFilteringButton;



    @FindBy(css="#product_filter_form_brands > div.js-product-filter-choice-title.box-filter__item__title")
    private WebElement publisherButton;



    @FindBy(css="#product_filter_form_brands > div.box-filter__item__box > button")
    private WebElement enterPublisherFilteringButton;



    @FindBy(xpath = "//*[@id=\"js-search-autocomplete-input\"]")
    private WebElement searchBox;



    public AllBooksPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public BookPage goToBookPage(WebElement bookElement) {
        bookElement.click();
        return new BookPage(driver);
    }

    public WebElement getMinimalPriceInput() {
        return minimalPriceInput;
    }

    public WebElement getMaximalPriceInput() {
        return maximalPriceInput;
    }

    public String getPriceRangeString() {
        waitForElementVisibility(priceRangeString);
        return priceRangeString.getText();
    }

    public void clickPriceButton() {
        waitForElementVisibility(priceButton);
        priceButton.click();
    }

    public void clickAvailabilityOfBooksButton() {
        waitForElementVisibility(availabilityOfBooksButton);
        availabilityOfBooksButton.click();
    }

    public void waitForElementVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void setBookToCart(WebElement setToCartButton) {
        waitForElementVisibility(setToCartButton);
        setToCartButton.click();
    }

    public void selectContinueShop() {
        waitForElementVisibility(continueShopButton);
        continueShopButton.click();
    }

    public CartPage selectGoToCart() {
        waitForElementVisibility(goToCartButton);
        goToCartButton.click();
        return new CartPage(driver);
    }

    public CartPage goCartNav() {
        waitForElementVisibility(cartNav);
        cartNav.click();
        return new CartPage(driver);
    }

    public void clickAvailabilityCheckBox() {
        waitForElementVisibility(availabilityCheckBox);
        availabilityCheckBox.click();
    }

    public void clickEnterAvailabilityFilteringButton() {
        waitForElementVisibility(enterAvailabilityFilteringButton);
        enterAvailabilityFilteringButton.click();
    }

    public void clickPublisherButton() {
        waitForElementVisibility(publisherButton);
        publisherButton.click();
    }

    public WebElement getPublisherButton() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        // Wait for the overlay to disappear
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".in-overlay__in")));

        WebElement boxFilterItem = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#product_filter_form_brands > div.js-product-filter-choice-title.box-filter__item__title")));
        return boxFilterItem;
    }

    public void clickEnterPublisherFilteringButton() {
         waitForElementVisibility(enterPublisherFilteringButton);
         enterPublisherFilteringButton.click();

    }

    public WebElement getSearchBox() {
        waitForElementVisibility(searchBox);
        return searchBox;
    }
}
