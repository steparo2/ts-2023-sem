package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {

    private WebDriver driver;

    @FindBy(css = "#header > div.web__header > div > header > div.header__combo > div.header__user.header__user--login > ul > li > a")
    private WebElement loginButton;

    @FindBy(css = "#js-window > div > form > div.box-login > div:nth-child(1) > input")
    private WebElement usernameField;

    @FindBy(css = "#js-window > div > form > div.box-login > div:nth-child(2) > input")
    private WebElement passwordField;

    @FindBy(css = "#js-window > div > form > div.box-login > div.box-login__item.box-login__item--submit > button")
    private WebElement prihlasitSeButton;

    @FindBy(css = "#header > div.web__header > div > header > div.header__combo > div.header__user > ul > li > a > div > div")
    private WebElement username;

    @FindBy(css = "body > div.web__in.on-homepage > div:nth-child(2) > div > div > div > div:nth-child(3) > h2 > span:nth-child(2) > a")
    private WebElement allBooks;

    @FindBy(css = "body > div.box-cookies.js-cookies-consent-bar.hidden-print > div > div.box-cookies__action > a.js-cookies-consent-button.btn.btn--small.box-cookies__action__btn.box-cookies__action__btn--accept.btn--primary")
    private WebElement acceptCookiesButton;

    @FindBy(css = "#header > div.web__header > div > header > div.header__combo > div.header__shopping-list > ul > li > a")
    private WebElement favoriteBooksNav;


    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public WishlistPage goToFavoritesPage() {
        waitForElementVisibility(favoriteBooksNav);
        favoriteBooksNav.click();
        return new WishlistPage(driver);
    }

    public void clickLoginButton() {
        WebDriverWait wait = new WebDriverWait(driver, 10); // ожидание до 10 секунд
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("js-overlay"))); // ожидание исчезновения оверлея
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#header > div.web__header > div > header > div.header__combo > div.header__user.header__user--login > ul > li > a")));
        loginButton.click();
    }

    public void enterCredentials(String username, String password) {
        waitForElementVisibility(usernameField);
        usernameField.sendKeys(username);
        waitForElementVisibility(passwordField);
        passwordField.sendKeys(password);
        prihlasitSeButton.click();
    }

    public WebElement getUsernameElement() {
        return username;
    }

    public void acceptCookies(){
        acceptCookiesButton.click();
    }

    public void waitForElementVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForElementTextToChange(WebElement element, String expectedText) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return element.getText().equals(expectedText);
            }
        });
    }

    public void login(){
        clickLoginButton();
        enterCredentials("markvertolet@gmail.com","B7surkHnRPbbiYz");
    }

    public void loginWithCsv(String username, String password){
        clickLoginButton();
        enterCredentials(username,password);
    }

    public void invalidLogin(){
        clickLoginButton();
        enterCredentials("sekapaka123@gmail.com","burmandetns");
    }


    public AllBooksPage goToAllBooksPage() {
        waitForElementVisibility(allBooks);
        driver.navigate().refresh();
        waitForElementVisibility(allBooks);
        allBooks.click();

        // Вернуть новый объект класса models.BookPage
        return new AllBooksPage(driver);
    }
}
