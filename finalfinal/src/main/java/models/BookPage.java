package models;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookPage {
    private WebDriver driver;
    @FindBy(css = "#js-fixed-header > div.web__header > div > header > div.header__logo > a")
    private WebElement mainPageElement;
    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/div[2]/div[1]/div[2]/div[2]/div[2]/div/div[2]/button")
    private WebElement wishlist;
    @FindBy(css = "body > div.web__in > div:nth-child(2) > div > div > div > div:nth-child(2) > div.js-box-detail.box-detail > div.box-detail__info > div.js-product-detail-main-add-to-cart-wrapper.box-detail-add.box-detail__info__add > div.box-detail-add__row.box-detail-add__row--tools > div > div.js-shopping-list.box-detail-shopping-list.box-detail-add__tools__button.box-detail-add__tools__button--shopping-list > ul > li:nth-child(1) > a.js-link-ajax-action.box-detail-shopping-list__dialog__item__link.box-detail-shopping-list__dialog__item__link--add")
    private WebElement favorite;
    @FindBy(css = "#js-fixed-header > div.web__header > div > header > div.header__combo > div.header__shopping-list > ul > li > a")
    private WebElement favoriteBooksNav;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/div[2]/div[1]/div[2]/div[2]/div[1]/div/div[3]/form/button")
    private WebElement addToCartButton;

    @FindBy(xpath = "//*[@id=\"js-window\"]/div[1]/a")
    private WebElement goToCartPageButton;

    public BookPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WishlistPage goToFavorites(){
        favoriteBooksNav.click();
        return new WishlistPage(driver);
    }

    public MainPage goToMainPage() {
        waitForElementVisibility(mainPageElement);
        mainPageElement.click();
        return new MainPage(driver);
    }

    public void setToWishList() {
        waitForElementVisibility(wishlist);
        wishlist.click();
        waitForElementVisibility(favorite);
        favorite.click();

    }

    public void waitForElementVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void clickAddToCart(){
        waitForElementVisibility(addToCartButton);
        addToCartButton.click();
    }

    public CartPage goToCartInDialogMenu(){
        waitForElementVisibility(goToCartPageButton);
        goToCartPageButton.click();
        return new CartPage(driver);
    }


}
