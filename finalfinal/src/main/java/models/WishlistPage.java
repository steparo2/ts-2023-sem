package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WishlistPage {
    private WebDriver driver;


    @FindBy(css = "#js-fixed-header > div.web__header > div > header > div.header__logo > a")
    private WebElement mainPageElement;


    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div[1]/div[2]/div")

    private WebElement deleteWasSuccessfulMessage;


    @FindBy(css = "body > div.web__in.web__in--my-account > div:nth-child(2) > div > div > div.web__main__content.js-web-main-content > p")
    private WebElement emptyWishlistMessage;


    @FindBy(css = "body > div.web__in.web__in--my-account > div:nth-child(2) > div > div > div.web__main__content.js-web-main-content > div.box-order-type__wrap.shopping-lists__menu__wrap > form > div.box-order-type.shopping-lists__menu > a.box-order-type__item.shopping-lists__menu__item.active")
    private WebElement favoritesCount;

    public WishlistPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getFavoritesCount() {
        return favoritesCount;
    }

    public String getDeleteWasSuccessfulMessage() {
        waitForElementVisibility(deleteWasSuccessfulMessage);
        return deleteWasSuccessfulMessage.getText();
    }

    public String getEmptyWishlistMessage() {
        waitForElementVisibility(emptyWishlistMessage);

        return emptyWishlistMessage.getText();
    }


    public void clickDeleteBook() {
        WebElement deleteElement = driver.findElement(By.className("js-link-overlay"));
        waitForElementVisibility(deleteElement);
        deleteElement.click();
    }

    public void waitForElementVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public MainPage goToMainPage() {
        waitForElementVisibility(mainPageElement);
        mainPageElement.click();
        return new MainPage(driver);
    }


}
