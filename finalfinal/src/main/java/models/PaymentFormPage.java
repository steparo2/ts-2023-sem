package models;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaymentFormPage {
    private WebDriver driver;

    public PaymentFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterCredentialsForPayment() {
        WebDriverWait wait = new WebDriverWait(driver, 10); // set up our explicit wait

        WebElement firstNameField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("order_personal_info_form_firstName")));
        firstNameField.clear();
        firstNameField.sendKeys("Konstanin");

        WebElement lastNameField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("order_personal_info_form_lastName")));
        lastNameField.clear();
        lastNameField.sendKeys("Viburnum");

        WebElement telephoneField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("order_personal_info_form_telephone")));
        telephoneField.clear();
        telephoneField.sendKeys("+420773518125");

        WebElement streetField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("order_personal_info_form_street")));
        streetField.clear();
        streetField.sendKeys("Technicka 2");

        WebElement cityField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("order_personal_info_form_city")));
        cityField.clear();
//        cityField.sendKeys("Praha");

        WebElement postcodeField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("order_personal_info_form_postcode")));
        postcodeField.clear();
        postcodeField.sendKeys("16000");

        // Scroll down
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");

        // Check the checkbox
        WebElement termsAndConditionsCheckbox = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("order_personal_info_form_termsAndConditionsAgreement")));
        termsAndConditionsCheckbox.click(); // checks the checkbox



        // continue with the rest of the form fields similarly
    }


    public void waitForElementVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

}
