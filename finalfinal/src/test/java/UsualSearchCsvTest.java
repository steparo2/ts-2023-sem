import models.MainPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class UsualSearchCsvTest {

    private WebDriver driver;
    private MainPage mainPage;

    @BeforeEach
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/searchData.csv", encoding = "UTF-8")
    public void searchTest(String searchQuery) throws InterruptedException {

        Thread.sleep(2000);

        // Найти поле поиска и ввести строку поиска
        WebElement searchBox = driver.findElement(By.id("js-search-autocomplete-input"));

        searchBox.sendKeys(searchQuery);

        // Нажать Enter для выполнения поиска
        searchBox.sendKeys(Keys.RETURN);

        String bookName = driver.findElement(By.cssSelector("body > div.web__in > div:nth-child(2) > div > div > div > div.js-product-list-with-filter > div > div.js-product-list-with-paginator > ul > li:nth-child(1) > div > div.list-products-page__item__info__wrap > a > div > h2")).getText();

        Assertions.assertEquals(searchQuery, bookName);
        // Добавить проверки теста здесь
    }
}