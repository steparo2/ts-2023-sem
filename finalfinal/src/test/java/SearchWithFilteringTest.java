import models.AllBooksPage;
import models.MainPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import static org.junit.Assert.assertEquals;

public class SearchWithFilteringTest {



    private WebDriver driver;
    private MainPage mainPage;


    @Before
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }



    @Test
    public void searchWithChoosingPublisherAndAvailability() throws InterruptedException {
        MainPage mainPage = new MainPage(driver);
        AllBooksPage allBooksPage = mainPage.goToAllBooksPage();

        allBooksPage.clickAvailabilityOfBooksButton();
        allBooksPage.clickAvailabilityCheckBox();
        allBooksPage.clickEnterAvailabilityFilteringButton();

        WebElement publisherButton =  allBooksPage.getPublisherButton();
        publisherButton.click();

        Thread.sleep(2000);
        WebDriverWait wait = new WebDriverWait(driver, 10); // wait for up to 10 seconds
        WebElement fontanaCheckbox = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#product_filter_form_brands_1449")));
        fontanaCheckbox.click();

        // Выбрать чекбокс "crew"
        WebElement crewCheckbox = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#product_filter_form_brands_390")));
        crewCheckbox.click();

        allBooksPage.clickEnterPublisherFilteringButton();

        // Wait for availability text to be present
        WebElement availabilityElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[2]/div[3]/div/div[1]/span[2]/span[2]")));
        String actualAvailability = availabilityElement.getText();
        String expectedAvailability = "Ihned k odeslání";
        Assertions.assertEquals(expectedAvailability, actualAvailability);

        // Wait for publisher text to be present
        WebElement publisherElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[2]/div[3]/div/div[1]/span[3]/span[2]")));
        String actualPublisher = publisherElement.getText();
        String expectedPublisherCrew = "Crew";
        Assertions.assertEquals(expectedPublisherCrew, actualPublisher);

        // Do the same for the second publisher text
        WebElement publisherElement2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[2]/div[3]/div/div[1]/span[3]/span[3]")));
        String actualPublisher2 = publisherElement2.getText();
        String expectedPublisherFontana = "Fontána";
        Assertions.assertEquals(expectedPublisherFontana, actualPublisher2);


    }
}