import models.AllBooksPage;
import models.MainPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchWithFilteringCsvTest {

    private WebDriver driver;
    private MainPage mainPage;


    @BeforeEach
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }


    @ParameterizedTest
    @CsvFileSource(resources = "/searchDataWithFiltering.csv", encoding = "UTF-8", numLinesToSkip = 1)
    public void searchWithPriceRange(int minPrice, int maxPrice) {
        AllBooksPage allBooksPage = mainPage.goToAllBooksPage();
        allBooksPage.clickPriceButton();

        // Clear the input fields before entering new values
        WebElement minimalPriceInput = allBooksPage.getMinimalPriceInput();
        minimalPriceInput.clear();
        minimalPriceInput.sendKeys(String.valueOf(minPrice));

        WebElement maximalPriceInput = allBooksPage.getMaximalPriceInput();
        maximalPriceInput.clear();
        maximalPriceInput.sendKeys(String.valueOf(maxPrice));

        String actualPriceRange = allBooksPage.getPriceRangeString();


        String expectedPriceRange = "Cena od " + minPrice + " Kč do " + maxPrice + " Kč";

        Assertions.assertEquals(expectedPriceRange, actualPriceRange);


    }
}
