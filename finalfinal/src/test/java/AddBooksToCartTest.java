import models.AllBooksPage;
import models.CartPage;
import models.MainPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddBooksToCartTest {
    private static final Logger logger = LogManager.getLogger(AddBooksToWishlistTest.class);
    private WebDriver driver;
    private MainPage mainPage;


    @Before
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }


    @Test
    public void addBooksToCart() {
        WebDriverWait wait = new WebDriverWait(driver, 10); // wait for up to 10 seconds


        AllBooksPage allBooksPage = mainPage.goToAllBooksPage();

        WebElement addBook1Button = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[2]/div[3]/div/div[3]/ul/li[3]/div/div[2]/div/div[3]/form/button"));

        String expectedFirstBookName = driver.findElement(By.cssSelector("body > div.web__in > div:nth-child(2) > div > div > div > div.box-list > div.js-product-list-with-filter > div > div.js-product-list-with-paginator > ul > li:nth-child(3) > div > div.list-products-page__item__info__wrap > a > div > h2")).getText();
        allBooksPage.setBookToCart(addBook1Button);


        allBooksPage.selectContinueShop();

        WebElement addBook2Button = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[2]/div[3]/div/div[3]/ul/li[4]/div/div[2]/div/div[3]/form/button"));

        String expectedSecondBookName = driver.findElement(By.cssSelector("body > div.web__in > div:nth-child(2) > div > div > div > div.box-list > div.js-product-list-with-filter > div > div.js-product-list-with-paginator > ul > li:nth-child(4) > div > div.list-products-page__item__info__wrap > a > div > h2")).getText();


        allBooksPage.setBookToCart(addBook2Button);

        CartPage cartPage = allBooksPage.selectGoToCart();

        String actualSecondBookName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.web__in.web__in--order-process > div:nth-child(2) > div > div > div > form > table > tbody > tr:nth-child(2) > td.table-cart__cell.table-cart__cell--name.js-cart-item-name > a"))).getText();
        String actualFirstBookName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.web__in.web__in--order-process > div:nth-child(2) > div > div > div > form > table > tbody > tr:nth-child(1) > td.table-cart__cell.table-cart__cell--name.js-cart-item-name > a"))).getText();



        Assert.assertEquals(expectedSecondBookName, actualFirstBookName);
        Assert.assertEquals(expectedFirstBookName, actualSecondBookName);


    }
}



