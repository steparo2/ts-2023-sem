import models.MainPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginTest {

    private WebDriver driver;
    private MainPage mainPage;



    @Before
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);

    }

    @Test
    public void testLogin() {
        mainPage.acceptCookies();
        mainPage.login();

        String expectedName = "Konstanin";

        WebElement usernameElement = mainPage.getUsernameElement();
        mainPage.waitForElementTextToChange(usernameElement, expectedName);

        String actualName = usernameElement.getText();
        Assert.assertEquals(expectedName, actualName);
    }


    @Test
    public void invalidLogin() {
        mainPage.acceptCookies();
        mainPage.invalidLogin();

        String expectedMessage = "Zadali jste nesprávný e-mail nebo heslo. Využijte, prosím, funkci \"Zapomněli jste své heslo?\".";

        WebElement message = driver.findElement(By.cssSelector("#js-window > div > div"));

        waitForElementVisibility(message);

        String actualMessage = message.getText();

        Assert.assertEquals(expectedMessage,actualMessage);
    }


    public void waitForElementVisibility(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }





    @After
    public void tearDown() {
        driver.quit();
    }
}
