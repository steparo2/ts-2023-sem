import models.MainPage;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginCsvTest {
    private WebDriver driver;
    private MainPage mainPage;


    @BeforeEach
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);

    }


    @ParameterizedTest
    @CsvFileSource(resources = "loginData.csv", numLinesToSkip = 1)
    public void testLogin(String login, String password, String name) {
        mainPage.acceptCookies();
        mainPage.loginWithCsv(login, password);

        WebElement usernameElement = mainPage.getUsernameElement();
        mainPage.waitForElementTextToChange(usernameElement, name);

        String actualName = usernameElement.getText();
        Assertions.assertEquals(name, actualName);
    }

}
