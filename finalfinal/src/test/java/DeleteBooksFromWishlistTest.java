import models.MainPage;
import models.WishlistPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DeleteBooksFromWishlistTest {

    private static final Logger logger = LogManager.getLogger(DeleteBooksFromWishlistTest.class);

    private WebDriver driver;
    private MainPage mainPage;


    @Before
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }

    @Test
    public void deleteBooksFromWishlist() {
        logger.info("deleteBooksFromWishlist test has started.");
        WishlistPage wishlistPage= mainPage.goToFavoritesPage();
        wishlistPage.clickDeleteBook();

        String expected1 = "Nákupní seznam byl smazán.";
        String expected2 = "Nákupní seznam je prázdný.";

        String actual1 = wishlistPage.getDeleteWasSuccessfulMessage();
        String actual2 = wishlistPage.getEmptyWishlistMessage();

        Assert.assertEquals(expected1, actual1);
        Assert.assertEquals(expected2, actual2);

//        logger.info("Test DeleteBooksFromWishlist completed.\nExpected_1 : "+expected1 +  " Actual_1: " +actual1 );
//        logger.info("Test DeleteBooksFromWishlist completed.\nExpected_2 : "+expected2 +  " Actual_2: " +actual2 );


    }
}
