import models.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddBooksToCartThenPayTest {

    private static final Logger logger = LogManager.getLogger(AddBooksToWishlistTest.class);
    private WebDriver driver;
    private MainPage mainPage;


    @Before
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }


    @Test
    public void addBooksToCart() {

        AllBooksPage allBooksPage = mainPage.goToAllBooksPage();

        WebElement addBook1Button = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[2]/div[3]/div/div[3]/ul/li[3]/div/div[2]/div/div[3]/form/button"));


        allBooksPage.setBookToCart(addBook1Button);

        allBooksPage.selectContinueShop();

        WebElement addBook2Button = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[2]/div[3]/div/div[3]/ul/li[4]/div/div[2]/div/div[3]/form/button"));

        allBooksPage.setBookToCart(addBook2Button);

        CartPage cartPage = allBooksPage.selectGoToCart();

        PaymentPage paymentPage = cartPage.goToPaymentPage();

        PaymentFormPage paymentFormPage= paymentPage.goToPaymentForm();

        paymentFormPage.enterCredentialsForPayment();

        WebElement submitButton = driver.findElement(By.id("order_personal_info_form_save"));

        String currentUrl = driver.getCurrentUrl();
        // try to click the button
        submitButton.click();
        // check that the URL did not change
        Assert.assertEquals(currentUrl, driver.getCurrentUrl());





    }
}
