import models.AllBooksPage;
import models.MainPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertTrue;


import java.util.List;

public class SearchWithSortingTest {

    private static final Logger logger = LogManager.getLogger(AddBooksToWishlistTest.class);
    private WebDriver driver;
    private MainPage mainPage;


    @Before
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }

    @Test
    public void searchWithLowestPriceFoundedBook() {
        AllBooksPage allBooksPage = mainPage.goToAllBooksPage();
        WebElement searchBox = allBooksPage.getSearchBox();

        searchBox.sendKeys("the witcher");

        // Нажать Enter для выполнения поиска
        searchBox.sendKeys(Keys.RETURN);


        WebDriverWait wait = new WebDriverWait(driver, 10); // wait for up to 10 seconds

        WebElement lowestPriceFilter =  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div[2]/div/div/div/div[4]/div/div[2]/div[2]/a[4]")));
        lowestPriceFilter.click();
        // ожидание появления первого элемента списка книг после применения фильтра
        WebElement firstBookPriceElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[contains(@class,'js-product-list')]/li[1]//div[contains(@class,'list-products__item__action__price__item--main')]")));
        String firstBookPriceString = firstBookPriceElement.getText().replaceAll("[^\\d]", ""); // извлечение числа из строки
        int firstBookPrice = Integer.parseInt(firstBookPriceString);

        // получение списка всех элементов после применения фильтра
        List<WebElement> bookPriceElements = driver.findElements(By.xpath("//ul[contains(@class,'js-product-list')]//div[contains(@class,'list-products__item__action__price__item--main')]"));
        for(WebElement bookPriceElement : bookPriceElements) {
            String bookPriceString = bookPriceElement.getText().replaceAll("[^\\d]", ""); // извлечение числа из строки
            int bookPrice = Integer.parseInt(bookPriceString);
            assertTrue(firstBookPrice <= bookPrice);
        }




    }
}
