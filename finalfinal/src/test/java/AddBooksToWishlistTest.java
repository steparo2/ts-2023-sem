import models.AllBooksPage;
import models.BookPage;
import models.MainPage;
import models.WishlistPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AddBooksToWishlistTest {
    private static final Logger logger = LogManager.getLogger(AddBooksToWishlistTest.class);


    private WebDriver driver;
    private MainPage mainPage;

    private WishlistPage wishlistPage;



    @Before
    public void setUp() {
        Dimension newDimension = new Dimension(1920, 1080);
        driver = new ChromeDriver();
        driver.manage().window().setSize(newDimension);
        driver.get("https://www.knihy.cz/");
        mainPage = new MainPage(driver);
        mainPage.acceptCookies();
        mainPage.login();
    }
    @AfterEach
    public void closeWindow() {driver.close();}

    public WishlistPage setUpBooksToWishlist(){

        AllBooksPage allBooksPage = mainPage.goToAllBooksPage();

        WebElement book1 = driver.findElement(By.cssSelector("body > div.web__in > div:nth-child(2) > div > div > div > div.box-list > div.js-product-list-with-filter > div > div.js-product-list-with-paginator > ul > li:nth-child(1) > div"));
        BookPage book1Page= allBooksPage.goToBookPage(book1);
        book1Page.setToWishList();

        book1Page.goToMainPage();

        mainPage.goToAllBooksPage();

        WebElement book2 = driver.findElement(By.cssSelector("body > div.web__in > div:nth-child(2) > div > div > div > div.box-list > div.js-product-list-with-filter > div > div.js-product-list-with-paginator > ul > li:nth-child(3) > div > div.list-products-page__item__info__wrap > a"));
        BookPage book2Page = allBooksPage.goToBookPage(book2);
        book2Page.setToWishList();

        return book2Page.goToFavorites();

    }

    @Test
    public void addBooksToWishlist() {

        logger.info("Starting addBooksToWishlist test.");

        WishlistPage wishlistPage = setUpBooksToWishlist();

        String expected = "Oblíbené (2)";
        String actual = wishlistPage.getFavoritesCount().getText();

        Assert.assertEquals(expected, actual);
        logger.info("Test addBooksToWishlis completed. EXPECTED : " + expected + " ACTUAL: " + actual );

    }


}
